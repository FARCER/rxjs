// import {Observable, of} from "rxjs";
//
// const sequence$: Observable<string | number> = of(...[1, 2, 3]);
//
// sequence$.subscribe((value: string | number) => {
//     console.log(value);
// }, () => {
// }, () => {
//     console.log('complete')
// });

// import {Observable, Observer} from "rxjs";
//
// const sequence$: Observable<number> = new Observable((observer: Observer<number>) => {
//         let count: number = 0;
//         setInterval(() => {
//             count++;
//             observer.next(count);
//         }, 1000)
//     }
// );
//
// sequence$.subscribe((value: string | number) => {
//     console.log(`Sub 1 ${value}`)
// }, () => {
// }, () => {
//     console.log('complete')
// });
//
// setTimeout(() => {
//     sequence$.subscribe((value: string | number) => {
//         console.log(`Sub 2 ${value}`)
//     }, () => {
//     }, () => {
//         console.log('complete')
//     })
// }, 5000);


// import {fromEvent, Observable} from "rxjs";
//
// const sequence$: Observable<MouseEvent> = fromEvent(document, 'click') as Observable<MouseEvent>;
//
// sequence$.subscribe((value: MouseEvent) => {
//     console.log(`Sub 1 ${value.clientX}`, value)
// }, () => {
// }, () => {
//     console.log('complete')
// });
//
// setTimeout(() => {
//     sequence$.subscribe((value: MouseEvent) => {
//         console.log(`Sub 2 ${value.clientX}`, value)
//     }, () => {
//     }, () => {
//         console.log('complete')
//     })
// }, 5000);


// import {interval, Observable} from "rxjs";
// import {map} from "rxjs/operators";
//
// const sequence1$: Observable<number> = interval(1000);
//
// const sequence2$: Observable<number> = sequence1$
//     .pipe(
//         map((x: number) => x * 2)
//     );
//
// sequence2$.subscribe((value: number) => console.log(value));

// import {interval, Observable} from "rxjs";
// import {map} from "rxjs/operators";
// import {tap} from "rxjs/internal/operators/tap";
//
// const sequence1$: Observable<number> = interval(1000);
//
// const sequence2$: Observable<number> = sequence1$
//     .pipe(
//         tap((x: number) => {
//             console.log('From console', x);
//             return x * 2
//         }),
//         map((x: number) => {
//             return x * 2
//         })
//     );
//
// sequence2$.subscribe((value: number) => console.log(value));


// import {interval, Observable} from "rxjs";
// import {skip, take} from "rxjs/operators";
//
// const sequence1$: Observable<number> = interval(1000);
//
// const sequence2$: Observable<number> = sequence1$
//     .pipe(
//         take(3),
//         skip(2)
//     );
//
// sequence2$.subscribe((value: number) => console.log(value), () => {
// }, () => {
//     console.log('complete')
// });


// import {concat, interval, Observable} from "rxjs";
// import {skip, take} from "rxjs/operators";
//
// const sequence1$: Observable<number> = interval(1000).pipe(take(4));
// const sequence2$: Observable<number> = interval(1000).pipe(skip(6), take(4));
//
//
// concat(sequence1$, sequence2$).subscribe((value) => console.log(value), () => {
// }, () => {
//     console.log('complete')
// });


// import {combineLatest, interval, Observable} from "rxjs";
// import {take} from "rxjs/operators";
//
// const sequence1$: Observable<number> = interval(500).pipe(take(4));
// const sequence2$: Observable<number> = interval(300).pipe(take(5));
//
//
// combineLatest(sequence1$, sequence2$).subscribe((value) => console.log(value), () => {
// }, () => {
//     console.log('complete')
// });

// import {fromEvent, Observable} from "rxjs";
// import {map} from "rxjs/operators";
// import {zip} from "rxjs/internal/observable/zip";
//
// const touchStart$: Observable<number> = getX(fromEvent(document, 'touchstart') as Observable<TouchEvent>);
// const touchEnd$: Observable<number> = getX(fromEvent(document, 'touchend') as Observable<TouchEvent>);
// const swipe$: Observable<number> = swipe(zip(touchStart$, touchEnd$));
//
// function getX(source$: Observable<TouchEvent>): Observable<number> {
//     return source$
//         .pipe(
//             map(({changedTouches}: TouchEvent) => changedTouches[0].clientX)
//         )
// }
//
// function swipe(source$: Observable<[number, number]>): Observable<number> {
//     return source$.pipe(
//         map(([startX, endX]: [number, number]) => {
//             return startX - endX;
//         })
//     );
// }
//
//
//
//
// swipe$.subscribe((direction: number) => {
//     if (direction > 0) {
//         console.log('Swipe left');
//         return;
//     }
//     console.log('Swipe Right');
// });


// import {interval, Observable} from "rxjs";
// import {map, take} from "rxjs/operators";
// import {of} from "rxjs/internal/observable/of";

// class SkipLimitSubscriber extends Subscriber<number> {
//
//     private _count: number = 1;
//     private _interval: number = 1;
//
//     public constructor(
//         subscriber: Subscriber<number>,
//         private _skip: number,
//         private _limit: number) {
//         super(subscriber);
//     }
//
//     protected _next(value: number): void {
//         const borderLow: number = this._interval * (this._skip + this._limit) - this._limit;
//         const borderHight: number = borderLow + this._limit;
//         if (borderLow < this._count && this._count <= borderHight) {
//             this.destination && this.destination.next && this.destination.next(value);
//             this._count++;
//             if (borderHight < this._count) {
//                 this._interval++;
//             }
//             return
//         }
//         this._count++;
//     }
//
// }
//
// function skipLimit(skip: number, limit: number) {
//     return (source: Observable<number>): Observable<number> => {
//         return source.lift({
//             call(subscriber: Subscriber<number>): void {
//                 source.subscribe(new SkipLimitSubscriber(subscriber, skip, limit))
//             }
//         })
//     }
// }
// interval(1000)
//     .pipe(skipLimit(3, 4))
//     .subscribe((value: number) => {
//         console.log(value)
//     });


// const sequence$: Observable<number> = interval(1000)
//     .pipe(
//         take(4)
//     );
//
// const highOrdersequence$ = sequence$.pipe(
//     switchMap(() => of(1, 2))
// );
//
// highOrdersequence$.subscribe((value) => {
//     console.log(value)
// });

// const clickSequence$ = fromEvent(document, 'click');
//
// function perforemedRequest() {
//     return fetch('https://jsonplaceholder.typicode.com/users/1')
//         .then(res => res.json())
// }
//
// const emailSequence$ = clickSequence$
//     .pipe(switchMap(_click => perforemedRequest()))
//     .subscribe(user => {
//         console.log(user.email)
//     });


// import {ReplaySubject} from "rxjs";
//
// const sequence$$ = new ReplaySubject();
// sequence$$.next(1);
// sequence$$.next(2);
// sequence$$.next(3);
// sequence$$.next(4);
// sequence$$.next(5);
// sequence$$.next(6);
// sequence$$.next('angular');
//
// setTimeout(() => {
//     sequence$$.next('Angular !!!!')
// }, 5000);
//
// setTimeout(() => {
//     sequence$$.subscribe(value => console.log('Sub2', value));
// }, 7000);


// import {interval, Observable, of} from "rxjs";
// import {zip} from "rxjs/internal/observable/zip";
// import {catchError, map, switchMap} from "rxjs/operators";
//
// const sequence1$: Observable<number> = interval(500);
// const sequence2$: Observable<number | string> = of('1', '2', '3', 4, '5','6','7');
//
// zip(sequence1$, sequence2$)
//     .pipe(
//         // @ts-ignore
//         switchMap(([x, y]: [number, string | number]) => {
//             return of(null)
//                 .pipe(
//                     map(() => {
//                         return (y as any).toUpperCase();
//                     }),
//                     catchError((err) => {
//                         console.log('Error 1', err);
//                         return of(0)
//                     })
//                 )
//         })
//     ).subscribe((value: any) => console.log(value),
//     null,
//     // (err: Error) => {
//     //     console.log('ERR', err);
//     // },
//     () => {
//         console.log('complete')
//     });
